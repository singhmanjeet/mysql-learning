# MySQL Database

## Create database

```
CREATE DATABASE IF NOT EXISTS dev_db;
```

## Show Database

```
SHOW DATABASES;
```

## Select Particular Database

```
USE dev_db;
```

## Drop Database

```
DROP DATABASE IF EXISTS dev_db_copy;  
```