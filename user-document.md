# USER Based Query

## create New USER

```
CREATE USER IF NOT EXISTS dev@localhost IDENTIFIED BY '123456';  # NOTE dev-> user_name, localhost ->hostname, 123456 -> password

```

## GRANT ALL Permission to user

```
GRANT ALL PRIVILEGES ON instacrew.* TO 'dev'@'localhost';  # where instacrew ->database _name
```

## GRANT Specific Permissions to user

```
GRANT CREATE, SELECT, INSERT, DELETE, UPDATE,ALTER ON instacrew.* TO 'dev'@'localhost';  #where instacrew ->database _name
```

## DELETE USER

```
DROP USER 'dev'@'localhost' 
```

## SHOW ALL USERs

```
SELECT USER FROM mysql.user 
```

## Change Password

```
ALTER USER dev@localhost IDENTIFIED BY '1234';
```
